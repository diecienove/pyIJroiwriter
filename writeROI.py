__author__ = "Daniele"
__email__ = "diecienove@gmail.com"

import struct

# x=[10, 10, 256, 256]
# y=[10, 256, 256, 10]
def writePolygon(x,y,filename):
    version=227
    roitype=0 #poligon
    top=0
    left=0
    bottom=0
    right=0
    ncordinates=len(x)
    X1=0
    Y1=0
    X2=0
    Y2=0
    strokewidth=0
    shaperoisize=0.0
    strokecolor=0.0
    fillcolor=0.0
    subtype=0 #simple line, not image or arrow
    options=0 #interpolation
    arrowtype=b'\x00'
    arrowheadsize=b'\x00'
    roundearcsize=0
    position=2 #for multislices ROI, start from 1
    offset2=96 #magic number :D
    zero=b'\x00'

    data=b''

    data+=b'Iout' #0_3
    data+=version.to_bytes(2, 'big')#4_5
    data+=roitype.to_bytes(2, 'big')#6_7
    data+=top.to_bytes(2, 'big')#8_9
    data+=left.to_bytes(2, 'big')#10_11
    data+=bottom.to_bytes(2, 'big')#12_13
    data+=right.to_bytes(2, 'big')#14_15
    data+=ncordinates.to_bytes(2, 'big')#16_17
    data+=struct.pack("f", X1)#18_21
    data+=struct.pack("f", Y1)#22_25
    data+=struct.pack("f", X2)#26_29
    data+=struct.pack("f", Y2)#30_33
    data+=strokewidth.to_bytes(2, 'big')#34_35
    data+=struct.pack("f", shaperoisize)#36_39
    data+=struct.pack("f", strokecolor)#40_43
    data+=struct.pack("f", fillcolor)#44_47
    data+=subtype.to_bytes(2, 'big')#48_49
    data+=options.to_bytes(2, 'big')#50_51
    data+=arrowtype#52
    data+=arrowheadsize#53
    data+=roundearcsize.to_bytes(2, 'big')#54_55
    data+=position.to_bytes(4, 'big')#56_59
    pos0=0
    data+=pos0.to_bytes(4, 'big')#60_63

    #coords append
    data_XY=b''
    for i in range(len(x)):
        data_XY+=x[i].to_bytes(2,'big')

    for i in range(len(y)):
        data_XY+=y[i].to_bytes(2,'big')

    data+=data_XY

    f = open(filename, 'wb')
    f.write(data)
    f.close()